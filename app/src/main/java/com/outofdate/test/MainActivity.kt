package com.outofdate.test

import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.AdapterView
import android.widget.Button
import android.widget.Spinner
import org.joda.time.LocalDate

class MainActivity : AppCompatActivity() {

    private lateinit var apts: RecyclerView
    private lateinit var bedRoomsFilter: Spinner
    private val adapter = ApartmentsAdapter()

    private lateinit var toFilter: Button

    private lateinit var fromFilter: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        apts = findViewById(R.id.apt_list)
        bedRoomsFilter = findViewById(R.id.bedrooms_filter)
        fromFilter = findViewById(R.id.from_filter)
        toFilter = findViewById(R.id.to_filter)
        apts.layoutManager = LinearLayoutManager(this)
        apts.adapter = adapter
        bedRoomsFilter.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>) {

            }

            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                adapter.filterByBedrooms(position)
            }
        }
        val nowDate = LocalDate.now()
        val now = nowDate.toString("dd.MM.YY")
        fromFilter.text = now
        toFilter.text = now
        fromFilter.setOnClickListener {
            DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                fromFilter.text = dayOfMonth.toString().plus(".").plus(month).plus(".").plus(year)
            }, nowDate.year, nowDate.monthOfYear, nowDate.dayOfMonth).show()
        }
        toFilter.setOnClickListener {
            DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                toFilter.text = dayOfMonth.toString().plus(".").plus(month).plus(".").plus(year)
            }, nowDate.year, nowDate.monthOfYear, nowDate.dayOfMonth).show()
        }
    }
}
