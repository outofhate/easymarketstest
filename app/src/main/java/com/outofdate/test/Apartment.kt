package com.outofdate.test

import org.joda.time.LocalDate
import kotlin.math.absoluteValue

class Apartment(val beds: Int, random: java.util.Random) {

    var booked = false
    val availableFrom = LocalDate.now().plusDays((random.nextInt(20 - 5 + 1) + 5).absoluteValue)
    val availableTo = availableFrom.plusDays((random.nextInt(20 - 5 + 1) + 5).absoluteValue)

}
