package com.outofdate.test

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import org.joda.time.LocalDate

class ApartmentsAdapter : RecyclerView.Adapter<ApartmentViewHolder>() {

    private val apartments = ArrayList<Apartment>()
    private val filtered = ArrayList<Apartment>()
    private val bookedApartments = ArrayList<Apartment>()
    private val random: java.util.Random = java.util.Random(3)

    init {
        for (i in 0 until 4) {
            apartments.add(Apartment(1, random))
        }
        for (i in 0 until 3) {
            apartments.add(Apartment(2, random))
        }
        for (i in 0 until 3) {
            apartments.add(Apartment(3, random))
        }
        val i = random.nextInt()
        apartments.shuffle()
        filtered.addAll(apartments)
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ApartmentViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.apartment, parent, false)
        return ApartmentViewHolder(view)
    }

    override fun getItemCount(): Int {
        return filtered.size
    }

    override fun onBindViewHolder(holder: ApartmentViewHolder, pos: Int) {
        val apt = filtered[pos]
        holder.setBeds(apt.beds)
        holder.setAvailability(apt.availableFrom, apt.availableTo)
        holder.setBookListener(View.OnClickListener {
            apt.booked = true
            bookedApartments.add(apt)
            filtered.remove(apt)
            notifyDataSetChanged()
        })
    }

    fun filterByDateRange(from: LocalDate, to: LocalDate) {
        val filteredByDate = ArrayList<Apartment>()
        for (apt in filtered) {
            if ((from.isAfter(apt.availableFrom) && to.isBefore(apt.availableTo)) ||
                (from.isEqual(apt.availableFrom) && to.isEqual(apt.availableTo))
            ) {
                filteredByDate.add(apt)
            }
        }
        filtered.clear()
        filtered.addAll(filteredByDate)
    }

    fun filterByBedrooms(pos: Int) {
        filtered.clear()
        when (pos) {
            0 -> {
                for (apt in apartments) {
                    if (!apt.booked)
                        filtered.add(apt)
                }
            }
            else -> {
                for (apt in apartments) {
                    if (apt.beds == pos && !apt.booked) {
                        filtered.add(apt)
                    }
                }
            }
        }
        notifyDataSetChanged()
    }
}

class ApartmentViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun setBeds(beds: Int) {
        itemView.findViewById<TextView>(R.id.bedrooms).text =
                beds.toString()
                    .plus(" ")
                    .plus(itemView.context.getString(R.string.beds_count))
    }

    fun setBookListener(click: View.OnClickListener) {
        itemView.findViewById<Button>(R.id.book).setOnClickListener(click::onClick)
    }

    fun setAvailability(availableFrom: LocalDate, availableTo: LocalDate) {
        itemView.findViewById<TextView>(R.id.availability)
            .text = availableFrom.toString("dd.MM.YY").plus("-").plus(availableTo.toString("dd.MM.YY"))
    }
}